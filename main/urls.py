from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('project', views.project, name='project'),
    path('thankyou', views.thankyou, name='thankyou'),
]
